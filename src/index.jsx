
import ForgeUI, {render, Text, PortalRequestDetailPanel, useProductContext, useState, Table, Row, Cell, Link} from '@forge/ui';
import api, {route} from "@forge/api";

const getOtherRequests = async (context) => {
    const res = await api
        .asUser()
        .requestJira(
            route`/rest/servicedeskapi/request`
        );
    const data = await res.json();
    return data.values;
};


const App = () => {
    const context = useProductContext();
    const [data] = useState(async () => await getOtherRequests(context));

    console.log(data);

    const current_request = context.extensionContext.request.key;
    console.log(context.extensionContext.request.key);

    const data_latest = data.slice(0,6);
    const data_excluding_current_request = data_latest.filter((data_request) => {
        return data_request.issueKey != current_request;
    });
    console.log(data_excluding_current_request);

    return (
        <Table>
            {data_excluding_current_request.map(request_link => (
                <Row>
                    <Cell>
                        <Text><Link href={request_link._links.web}>{request_link.requestFieldValues[0].value}</Link></Text>
                    </Cell>
                    <Cell>
                        <Text>{request_link.currentStatus.status}</Text>
                    </Cell>
                </Row>
            ))}
        </Table>
  );
};

export const run = render(
  <PortalRequestDetailPanel>
    <App />
  </PortalRequestDetailPanel>
);
