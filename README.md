# JSM Recent Requests

This is an example [Forge](https://developer.atlassian.com/platform/forge/) app that displays latest 5 requests of the User in Jira Service Management [Customer Portal Request Detail Panel section](https://developer.atlassian.com/platform/forge/ui-kit-components/jira-service-management/portal-request-detail-panel/).

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository
2. Run `forge register` to register a new copy of this app to your developer account
3. Run `npm install` to install your dependencies
4. Run `forge deploy` to deploy the app into the default environment
5. Run `forge install` and follow the prompts to install the app

### Notes
- Use the `forge deploy` command when you want to persist code changes.
- Use the `forge install` command when you want to install the app on a new site.
- Once the app is installed on a site, the site picks up the new app changes you deploy without needing to rerun the install command.

### Debugging
You can use the `forge tunnel` command to run your Forge app locally.

## Usage
After the Forge App is installed, whenever a request is opened, a list of recent requests is displayed along with its status in [Customer Portal Request Detail Panel](https://developer.atlassian.com/platform/forge/ui-kit-components/jira-service-management/portal-request-detail-panel/) section. Here every request displayed in this section would be a link to its respective request, which when clicked redirects to the respective request page. This allows us to browse around the recent requests from the opened request page itself.

![All Requests Page](./images/requests1.png)

![Clicked on a Request](./images/requests2.png)

## Documentation

### The app's manifest.yml is based on one module:
1. The [jiraServiceManagement:portalRequestDetailPanel](https://developer.atlassian.com/platform/forge/manifest-reference/modules/jira-service-management-portal-request-detail-panel/
   ) module adds a panel to a portal request in side panel.
2. The content of the module is shown at the bottom of request side panel. This module can be used in Jira Service Management.

## Modules used in the app

- [Text](https://developer.atlassian.com/platform/forge/ui-components/text) component
- [Link](https://developer.atlassian.com/platform/forge/ui-kit-components/text/#link) component
- [Table](https://developer.atlassian.com/platform/forge/ui-components/table) component
- [PortalRequestDetailPanel](https://developer.atlassian.com/platform/forge/ui-kit-components/jira-service-management/portal-request-detail-panel/) component
- [useProductContext](https://developer.atlassian.com/platform/forge/ui-kit-hooks-reference/#useproductcontext) hook
- [useState](https://developer.atlassian.com/platform/forge/ui-kit-hooks-reference/#usestate) hook

## License

Copyright (c) 2022 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![From Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers.png)](https://www.atlassian.com)